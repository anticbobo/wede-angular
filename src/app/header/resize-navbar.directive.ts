import { Directive, Input, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appResizeNavbar]'
})
export class ResizeNavbarDirective {

  @Input() defaultTop: string = '-108px';
  @Input() topScrolled: string = '-89px';
  @HostBinding('style.top') setPositionTop: string;

  constructor() { }

  @HostListener('window:scroll', []) onWindowScroll() {
    if(window.pageYOffset > 50) {
      this.setPositionTop = this.topScrolled;
    } else {
      this.setPositionTop = this.defaultTop;
    }
  }

}
