import { Directive, HostBinding, HostListener, Inject, ViewChild, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[resizeHeader]'
})
export class ResizeHeaderDirective {
  
  @HostBinding('class.nav-resized') isResized = false;
  @HostBinding('style.height') navHeight: string;

  constructor() { }
  
  @HostListener('window:scroll', []) onWindowScroll() {
    if(window.pageYOffset > 50) {
      this.isResized = true;
    }
    else {
      this.isResized = false;
    }
  }

}