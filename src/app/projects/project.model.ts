export class Project {
    constructor(
        public name: string, 
        public img: { 
            img1: string, 
            img2: string, 
            img3: string,
            img4: string,
            img5: string,
            img6: string
        }, 
        public cms: string, 
        public description: string, 
        public url: string, 
        public techUsed: Technology
    ) {}
}

export class Technology {
    constructor(
        public frontEnd: string,
        public backEnd: string
    ) {}
}