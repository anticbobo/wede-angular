import { Component, OnInit, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Project } from './../project.model';
import { ProjectDataService } from './../project-list/project-data.service';
import { NgxCarousel } from 'ngx-carousel';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss']
})
export class ProjectDetailsComponent implements OnInit {

  @Input() project: Project;
  public projectImg: {};
  public imgSlider: NgxCarousel;
  public projectTechUsed: {};
  objectKeys = Object.keys;

  constructor(
    public dialogRef: MatDialogRef<ProjectDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public projectData: ProjectDataService) { }

  onCloseDialog(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.projectData.currentProject.subscribe(project => this.project = project);
    this.projectData.currentProject.subscribe(project => this.projectImg = project.img);
    this.projectData.currentProject.subscribe(project => this.projectTechUsed = project.techUsed);
    this.imgSlider = {
      grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
      speed: 1000,
      interval: 3000,
      point: {
        visible: true,
        pointStyles: `
          .ngxcarouselPoint {
            list-style-type: none;
            text-align: center;
            padding: 12px;
            margin: 0;
            white-space: nowrap;
            overflow: auto;
            box-sizing: border-box;
          }
          .ngxcarouselPoint li {
            display: inline-block;
            border-radius: 50%;
            background: #6b6b6b;
            padding: 5px;
            margin: 0 3px;
            transition: .4s;
          }
          .ngxcarouselPoint li.active {
              border: 2px solid rgba(0, 0, 0, 0.55);
              transform: scale(1.2);
              background: transparent;
            }
        `
      },
      load: 2,
      loop: true,
      touch: true,
      easing: 'ease',
      animation: 'lazy'
    };
  }

}