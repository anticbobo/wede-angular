import { Component, OnInit, Output, EventEmitter, Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Project } from '../project.model';
import { ProjectDataService } from './project-data.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit {
  projects: Project[];
  loading: boolean = true;
  @Output() projectWasSelected = new EventEmitter<Project>();

  constructor(private projectData: ProjectDataService, private http: Http) { }

  ngOnInit() {
    this.projectData.getProjects().subscribe(
      (projects: Project[]) => {
        this.projects = projects;
        this.loading = false;
      }
    )
  }

  projectIsSelected(project: Project) {
    this.projectWasSelected.emit(project);
  }

}
