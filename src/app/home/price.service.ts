import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class CryptoPrice {
    private currentPriceUrl = 'http://api.coindesk.com/v1/bpi/currentprice.json';
    /* private currentPriceUrl = './../data/btcprice.json'; */

    constructor(private http: Http) { }

    async getPrice(): Promise<number> {
        const response = await this.http.get(this.currentPriceUrl).toPromise();
        return response.json().bpi.GBP.rate;
    }
    async getTimeUpdated(): Promise<number> {
        const response = await this.http.get(this.currentPriceUrl).toPromise();
        return response.json().time.updated;
    }
}