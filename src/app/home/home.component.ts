<<<<<<< HEAD
import { Component, OnInit, NgModule } from '@angular/core';
import { NgxCarousel } from 'ngx-carousel';
import { Meta, Title } from '@angular/platform-browser';
=======
import { Component, OnInit } from '@angular/core';
>>>>>>> 5f99a174d711205fb885ee98bdcaad383dcb1fad

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
<<<<<<< HEAD
  imgags: Object[];

  public carouselBannerItems: Array<any> = [];
  public carouselBanner: NgxCarousel;

  constructor(private meta: Meta, private title: Title) {
    this.title.setTitle('Wede Tech - About us');
    this.meta.addTag({ name: 'author', content: 'Wede Tech'});
    this.meta.addTag({ name: 'description', content: 'Turn your website into a powerful marketing tool.'});
    this.meta.addTag({ name: 'twitter:card', content: 'summary_large_image' });
    this.meta.addTag({ name: 'twitter:site', content: '' });
    this.meta.addTag({ name: 'twitter:title', content: 'Wede Tech' });
    this.meta.addTag({ name: 'twitter:description', content: 'Turn your website into a powerful marketing tool.' });
    this.meta.addTag({ name: 'twitter:image', content: 'https://wede.tech/assets/img/logo-rebrand.png' });
    this.meta.addTag({ name: 'og:title', content: 'Wede Tech'});
    this.meta.addTag({ name: 'og:type', content: 'website'});
    this.meta.addTag({ name: 'og:url', content: 'https://wede.tech/'});
    this.meta.addTag({ name: 'og:image', content: 'https://wede.tech/assets/img/logo-rebrand.png'});
    this.meta.addTag({ name: 'og:description', content: 'Turn your website into a powerful marketing tool.'});
  }

  ngOnInit() {
    this.imgags = [
      {'imgPath': './../../assets/img/home/slider-bg-1.jpg', 'heading': 'We create the web', 'url': '/contact'},
      {'imgPath': './../../assets/img/home/slider-bg-3.jpg', 'heading': 'We love what we do', 'url': '/contact'},
      {'imgPath': './../../assets/img/home/slider-bg-5.jpg', 'heading': 'Think, than design', 'url': '/contact'},
    ];

    this.carouselBanner = {
      grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
      slide: 4,
      speed: 500,
      interval: 5000,
      point: {
        visible: true,
        pointStyles: `
          .ngxcarouselPoint {
            list-style-type: none;
            text-align: center;
            padding: 12px;
            margin: 0;
            white-space: nowrap;
            overflow: auto;
            position: absolute;
            width: 100%;
            bottom: 20px;
            left: 0;
            box-sizing: border-box;
          }
          .ngxcarouselPoint li {
            display: inline-block;
            border-radius: 999px;
            background: rgba(255, 255, 255, 0.55);
            padding: 5px;
            margin: 0 3px;
            transition: .4s ease all;
          }
          .ngxcarouselPoint li.active {
              background: white;
              width: 10px;
          }
        `
      },
      load: 2,
      custom: 'banner',
      touch: true,
      loop: true,
      easing: 'cubic-bezier(0, 0, 0.2, 1)'
    };
    this.carouselBannerLoad();
  }
  public carouselBannerLoad() {
    const len = this.carouselBannerItems.length;
    if (len <= 2) {
      for (let i = len; i < len + 3; i++) {
        this.carouselBannerItems.push(
          this.imgags[i]
        );
      }
    }
=======

  constructor() { }

  ngOnInit() {
>>>>>>> 5f99a174d711205fb885ee98bdcaad383dcb1fad
  }

}
