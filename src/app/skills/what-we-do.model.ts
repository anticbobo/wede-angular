export class WhatWeDo {
    constructor(public icon: string, public heading: string, public description: string) {}
}