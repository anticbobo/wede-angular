export class CircleParameters {
    constructor(
        public current: number,
        public max: number,
        public stroke: number,
        public radius: number,
        public semicircle: boolean,
        public rounded: boolean,
        public responsive: boolean,
        public clockwise: boolean,
        public color: string,
        public background: string,
        public duration: number,
        public animation: string,
        public animationDelay: number,
        public animations: string[],
        public gradient: boolean,
        public realCurrent: number,
        public title: string,
        public description: string) {}
}