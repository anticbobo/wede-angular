import { Component, OnInit } from '@angular/core';
<<<<<<< HEAD
import { CircleParameters } from './circle.model';
import { WhatWeDo } from './what-we-do.model';
import { Title, Meta } from '@angular/platform-browser';
=======
>>>>>>> 5f99a174d711205fb885ee98bdcaad383dcb1fad

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {
<<<<<<< HEAD
  params: CircleParameters[] = [
    new CircleParameters(80.2, 80.2, 8, 110, false, false, true, true, '#039be5', '#ffca28', 1000, 'linearEase', 500, [], false, 1000, 
      'Lines of code x1000', 'We have written over 80218 lines of well indented and readable code. After all, practice makes it perfect'),
    new CircleParameters(17, 18, 8, 110, false, false, true, true, '#039be5', '#ffca28', 1000, 'linearEase', 1000, [], false, 1000, 
      'Completed projects', 'We are proud of our completed projects. Our clients are proud too. Become our client and be proud of your website'),
      new CircleParameters(17, 18, 8, 110, false, false, true, true, '#039be5', '#ffca28', 1000, 'linearEase', 1500, [], false, 1000, 
      'Happy customers', 'It is our mission to make our clients happy. That is what, we in Wede Tech aim for.'),
    new CircleParameters(5, 5, 8, 110, false, false, true, true, '#039be5', '#ffca28', 1000, 'linearEase', 2000, [], false, 1000, 
      'Our team', 'Our team of professionals works hard delivering value to our clients. Our company supports and challenges them to be best in all areas of their life')
  ]
  ourServices: WhatWeDo[] = [
    new WhatWeDo('fa-diamond', 'Web design', 'Creating a beautiful design isn\'t just a creative job. It also means finding out what clients want'),
    new WhatWeDo('fa-desktop', 'UI/UX', 'This is where the magic happens. The UI and UX we deliver will make your customer fall in love with your website'),
    new WhatWeDo('fa-calculator', 'Programming', 'Do you want your website to think by itself, to have it\'s own mind? We can make a beautiful mind for your website'),
    new WhatWeDo('fa-handshake-o', 'Social media', 'A good way to promote your website and business is social media.')
  ]
  
  constructor(private meta: Meta, private title: Title) {
    this.title.setTitle('Wede Tech - Skills & Services');
    this.meta.addTag({ name: 'author', content: 'Wede Tech'});
    this.meta.addTag({ name: 'description', content: 'Turn your website into a powerful marketing tool.'});
    this.meta.addTag({ name: 'twitter:card', content: 'summary_large_image' });
    this.meta.addTag({ name: 'twitter:site', content: '' });
    this.meta.addTag({ name: 'twitter:title', content: 'Wede Tech - Services' });
    this.meta.addTag({ name: 'twitter:description', content: 'Turn your website into a powerful marketing tool.' });
    this.meta.addTag({ name: 'twitter:image', content: 'https://wede.tech/assets/img/logo-rebrand.png' });
    this.meta.addTag({ name: 'og:title', content: 'Wede Tech - Services'});
    this.meta.addTag({ name: 'og:type', content: 'website'});
    this.meta.addTag({ name: 'og:url', content: 'https://wede.tech/skills-services'});
    this.meta.addTag({ name: 'og:image', content: 'https://wede.tech/assets/img/logo-rebrand.png'});
    this.meta.addTag({ name: 'og:description', content: 'Turn your website into a powerful marketing tool.'});
  }

  ngOnInit() {
    
  }
  
  getOverlayStyle() {
      let isSemi = this.params[0].semicircle;
      let transform = (isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';
      return {
        'top': isSemi ? 'auto' : '50%',
        'bottom': isSemi ? '5%' : 'auto',
        'left': '50%',
        'transform': transform,
        '-moz-transform': transform,
        '-webkit-transform': transform,
        'font-size': this.params[0].radius / 2 + 'px'
      }
    
=======

  constructor() { }

  ngOnInit() {
>>>>>>> 5f99a174d711205fb885ee98bdcaad383dcb1fad
  }

}
