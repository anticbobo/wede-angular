import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
 
const headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});

@Injectable()
export class EmailService {
  private emailUrl = 'https://wede.mk/assets/contactProcess.php';
 
  constructor(private http: HttpClient) {}

  sendMessage(data: any): Observable<any> {
    return this.http.post(this.emailUrl, data, {headers: headers})
      .map(result => {
        console.log('Success from service', result)
      })
      .catch(error => {
        console.log('Error from service', error);
        return Observable.throw(error);
      })
  }

}