import { Component, OnInit } from '@angular/core';
import { Slide } from './slide.model';
import { NgxCarousel } from 'ngx-carousel';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.scss']
})
export class TestimonialsComponent implements OnInit {
 
  public carouselTileOneItems: Slide[] = [
    /* new Slide('http://placehold.it/600x400/888888', 'Biotek Solutions', 'biotek.com.mk', 
    'Nunc a ante ut libero efficitur aliquet sit amet sed leo. Morbi est elit, fermentum in aliquet faucibus, aliquet eu justo.', '1716602188596820', '', '', 5),
    new Slide('http://placehold.it/600x400/888888', 'Dimitri Voin', 'safewayautoinc.com', 
    'Nunc a ante ut libero efficitur aliquet sit amet sed leo. Morbi est elit, fermentum in aliquet faucibus, aliquet eu justo.', '', '', '', 5), */
    new Slide('http://placehold.it/600x400/888888', 'Vasil Andreevski', 'lukas.com.mk', 
    'Everything worked out perfectly thanks to the teams\' 5 star service and understanding of our needs. Thank you Wede Tech!', '', '', 'lucas-skopje', 5),
    new Slide('http://placehold.it/600x400/000000', 'Eko Republika', 'ekorepublika.mk', 
    'We got a great website with which we are proud of. The support Wede Tech is offering is outstanding. Every problem is solved swiftly and effectively.', '116317519045669', '', '', 5),
    new Slide('http://placehold.it/600x400/222222', 'Zoran Antic', 'interior.mk', 
    'Thank you so much to Wede Tech for making an absolutely amazing website for us. So many comments on how fantastic you were.', '1040097952698961', '', '', 5),
    new Slide('http://placehold.it/600x400/444444', 'Milan Angelovski', 'siketrejd.mk', 
    'Their proven experience and excellence is the reason why our company would recommend Wede Tech to anyone who is looking to create a customized website.', '100022284722368', '', '', 5),
    new Slide('http://placehold.it/600x400/666666', 'Luka Ivano', 'next-carrier.com', 
    'The only team that was able to find a solution for our needs. You made the application process much easier and the client more satisfied.', '356336028032513', '', '', 5),
    new Slide('http://placehold.it/600x400/111111', 'Zani Gelevska', 'zani.com.mk', 
    'Thanks to their knowledge and determination my website looks great and functions really good. I recommend anyone that is looking for a custom website to give them a call.', '1264618646', '', '', 5),
    new Slide('http://placehold.it/600x400/888888', 'Proke Kuzmanovski', 'globusevent.mk', 
    'Wede Tech did everything we asked in a timely matter. I will definitely be recommending him to other companies. Thanks for such good work.', '794296739', '', '', 5),
    new Slide('http://placehold.it/600x400/111111', 'Ana Serafimova', 'Gradinka Rosica', 
    'We have been delighted to cooperate with Wede Tech\'s team. The result is a website that the staff, children and parents like very much.', '', '', '', 5)
  ]
  public carouselTileOne: NgxCarousel;
  customStyle = {
    margin: "0 auto"
  }
  
  constructor(private meta: Meta, private title: Title) {
    this.title.setTitle('Wede Tech - Testimonials');
    this.meta.addTag({ name: 'author', content: 'Wede Tech'});
    this.meta.addTag({ name: 'description', content: 'Turn your website into a powerful marketing tool.'});
    this.meta.addTag({ name: 'twitter:card', content: 'summary_large_image' });
    this.meta.addTag({ name: 'twitter:site', content: '' });
    this.meta.addTag({ name: 'twitter:title', content: 'Wede Tech - Testimonials' });
    this.meta.addTag({ name: 'twitter:description', content: 'Turn your website into a powerful marketing tool.' });
    this.meta.addTag({ name: 'twitter:image', content: 'https://wede.tech/assets/img/logo-rebrand.png' });
    this.meta.addTag({ name: 'og:title', content: 'Wede Tech - Testimonials'});
    this.meta.addTag({ name: 'og:type', content: 'website'});
    this.meta.addTag({ name: 'og:url', content: 'https://wede.tech/testimonials'});
    this.meta.addTag({ name: 'og:image', content: 'https://wede.tech/assets/img/logo-rebrand.png'});
    this.meta.addTag({ name: 'og:description', content: 'Turn your website into a powerful marketing tool.'});
  }

  ngOnInit() {
    this.carouselTileOne = {
      grid: { xs: 1, sm: 2, md: 3, lg: 4, all: 0 },
      speed: 1500,
      interval: 3500,
      point: {
        visible: true,
        pointStyles: `
          .ngxcarouselPoint {
            list-style-type: none;
            text-align: center;
            padding: 12px;
            margin: 0;
            white-space: nowrap;
            overflow: auto;
            box-sizing: border-box;
          }
          .ngxcarouselPoint li {
            display: inline-block;
            border-radius: 50%;
            background: #6b6b6b;
            padding: 5px;
            margin: 0 3px;
            transition: .4s;
          }
          .ngxcarouselPoint li.active {
              border: 2px solid rgba(0, 0, 0, 0.55);
              transform: scale(1.2);
              background: transparent;
            }
        `
      },
      load: 2,
      loop: true,
      touch: true,
      easing: 'ease',
      animation: 'lazy'
    };
  }

}
