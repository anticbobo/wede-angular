export class Slide {
    constructor(
        public thumbnail: string, 
        public name: string, 
        public company: string, 
        public testimonial: string,
        public fbId: string,
        public twitterId: string,
        public skypeId: string,
        public rating: number
    ) {}
}