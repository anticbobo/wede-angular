import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import {
  MatSidenavModule, MatIconModule, MatToolbarModule, MatListModule, MatDialogModule, MatInputModule, MatSliderModule, MatButtonModule, MatSnackBarModule
} from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { AgmCoreModule } from '@agm/core';
import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms'
import { NgAutoCompleteModule } from "ng-auto-complete";
import { SlickModule } from 'ngx-slick';
import { AvatarModule } from 'ngx-avatar';
import { NgxCarouselModule } from 'ngx-carousel';
import { NgsRevealModule } from 'ng-scrollreveal';
import { RatingModule } from 'ngx-rating';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { SkillsComponent } from './skills/skills.component';
import { ProjectsComponent } from './projects/projects.component';
import { ProjectListComponent } from './projects/project-list/project-list.component';
import { ProjectItemComponent } from './projects/project-list/project-item/project-item.component';
import { ProjectDetailsComponent } from './projects/project-details/project-details.component';
import { ProjectDataService } from './projects/project-list/project-data.service';
import { ContactComponent } from './contact/contact.component';
import { EmailService } from './contact/email.service';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { ResizeHeaderDirective } from './header/resize-header.directive';
import { TeamMemberComponent } from './about/team-member/team-member.component';
import { ProjectFilterPipe } from './projects/project-list/project.pipe';
import { NotFoundComponent } from './not-found/not-found.component';
import { ResizeNavbarDirective } from './header/resize-navbar.directive';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent,
    data: { state: 'home' }
  },
  {
    path: 'about-us',
    component: AboutComponent,
    data: { state: 'about-us' }
  },
  {
    path: 'skills-services',
    component: SkillsComponent,
    data: { state: 'skills' }
  },
  {
    path: 'projects',
    component: ProjectsComponent,
    data: { state: 'projects' }
  },
  {
    path: 'testimonials',
    component: TestimonialsComponent,
    data: { state: 'testimonials' }
  },
  {
    path: 'contact-us',
    component: ContactComponent,
    data: { state: 'contact-us' }
  },
  { path: '**', component: NotFoundComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AboutComponent,
    SkillsComponent,
    ProjectsComponent,
    ProjectItemComponent,
    ContactComponent,
    TestimonialsComponent,
    ResizeHeaderDirective,
    TeamMemberComponent,
    ProjectDetailsComponent,
    ProjectListComponent,
    ProjectFilterPipe,
    NotFoundComponent,
    ResizeNavbarDirective
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'wede-app'}),
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatIconModule,
    MatToolbarModule,
    MatListModule,
    MatInputModule,
    MatSliderModule,
    MatButtonModule,
    MatSnackBarModule,
    NgbModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC9-B6N4GIjj-aS-fbbCBnczUhv6g7jQB4',
      libraries: ['places']
    }),
    RecaptchaModule.forRoot(),
    RecaptchaFormsModule,
    NgAutoCompleteModule,
    RoundProgressModule,
    SlickModule.forRoot(),
    AvatarModule,
    NgxCarouselModule,
    NgsRevealModule.forRoot(),
    RatingModule
  ], 
  exports: [
    RouterModule,
    BrowserAnimationsModule,
    CdkTableModule,
    MatDialogModule
  ],
  entryComponents: [ProjectsComponent, ProjectDetailsComponent],
  providers: [ProjectDataService, EmailService],
  bootstrap: [AppComponent]
})

export class AppModule { }