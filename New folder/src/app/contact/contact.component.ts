import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { EmailService } from './email.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { Meta, Title } from '@angular/platform-browser';

/* 
  Show errors when input is dirty or submitted
*/
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || isSubmitted));
  }
}

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  providers: [EmailService]
})
export class ContactComponent implements OnInit {
  title: string = 'WEDE TECH';
  lat: number = 47.602256;
  lng: number = 10.569477;
  zoom: number = 5;
  markers: {lat: number, lng: number, label?: string, draggable: boolean}[] = [
    {lat: 51.456285, lng: -0.218529, label: 'Wede Tech, London Office', draggable: false},
    {lat: 41.986921, lng: 21.439829, label: 'Wede Tech, Main Office, Skopje', draggable: false}
  ]
  styles = [
    {
      elementType: 'geometry',
      stylers: [{color: '#242f3e'}]
    },
    {
      elementType: 'labels.text.stroke', 
      stylers: [{color: '#242f3e'}]
    },
    {
      elementType: 'labels.text.fill',
      stylers: [{color: '#746855'}]
    },
    {
      featureType: 'administrative.locality',
      elementType: 'labels.text.fill',
      stylers: [{color: '#d59563'}]
    },
    {
      featureType: 'poi',
      elementType: 'labels.text.fill',
      stylers: [{color: '#d59563'}]
    },
    {
      featureType: 'poi.park',
      elementType: 'geometry',
      stylers: [{color: '#263c3f'}]
    },
    {
      featureType: 'poi.park',
      elementType: 'labels.text.fill',
      stylers: [{color: '#6b9a76'}]
    },
    {
      featureType: 'road',
      elementType: 'geometry',
      stylers: [{color: '#38414e'}]
    },
    {
      featureType: 'road',
      elementType: 'geometry.stroke',
      stylers: [{color: '#212a37'}]
    },
    {
      featureType: 'road',
      elementType: 'labels.text.fill',
      stylers: [{color: '#9ca5b3'}]
    },
    {
      featureType: 'road.highway',
      elementType: 'geometry',
      stylers: [{color: '#746855'}]
    },
    {
      featureType: 'road.highway',
      elementType: 'geometry.stroke',
      stylers: [{color: '#1f2835'}]
    },
    {
      featureType: 'road.highway',
      elementType: 'labels.text.fill',
      stylers: [{color: '#f3d19c'}]
    },
    {
      featureType: 'transit',
      elementType: 'geometry',
      stylers: [{color: '#2f3948'}]
    },
    {
      featureType: 'transit.station',
      elementType: 'labels.text.fill',
      stylers: [{color: '#d59563'}]
    },
    {
      featureType: 'water',
      elementType: 'geometry',
      stylers: [{color: '#17263c'}]
    },
    {
      featureType: 'water',
      elementType: 'labels.text.fill',
      stylers: [{color: '#515c6d'}]
    },
    {
      featureType: 'water',
      elementType: 'labels.text.stroke',
      stylers: [{color: '#17263c'}]
    }
  ]

  //generate form variables
  wtCForm: FormGroup;
  loading: boolean = false;
  name;
  email;
  phone;
  subject;
  message;
  matcher = new ErrorStateMatcher;

  public contactDetails: {icon: string, description: string, content1: string, content2: string}[] = [
    {"icon": "fa-map-o", "description": "address", "content1": "11th October St. 33A, Skopje 1000", "content2": ""},
    {"icon": "fa-clock-o", "description": "working hours", "content1": "Monday-Friday", "content2": "08:00 - 16:00 utc+2"},
    {"icon": "fa-envelope-open-o", "description": "email", "content1": "info@wede.tech", "content2": ""},
    {"icon": "fa-phone", "description": "call us", "content1": "Skopje: 389 70 259841", "content2": ""}
  ]

  constructor(private fb: FormBuilder, private http: HttpClient, private es: EmailService, private cFormInfo: MatSnackBar, private meta: Meta, private pageTitle: Title) {
    this.wtCForm = fb.group({
      name: ['', Validators.required],
      email: ['', [
        Validators.required,
        Validators.pattern('([a-z0-9][-a-z0-9_\+\.]*[a-z0-9])@([a-z0-9][-a-z0-9\.]*[a-z0-9]\.(arpa|root|aero|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)|([0-9]{1,3}\.{3}[0-9]{1,3}))')
      ]],
      phone: ['', [
        Validators.pattern('^[0-9]*$'),
        Validators.minLength(10)
      ]],
      subject: [''],
      message: ['', [
        Validators.required,
        Validators.minLength(20)
      ]],
      recaptcha: [null, Validators.required]
    })
    this.pageTitle.setTitle('Wede Tech - Contact us');
    this.meta.addTag({ name: 'author', content: 'Wede Tech'});
    this.meta.addTag({ name: 'description', content: 'Turn your website into a powerful marketing tool.'});
    this.meta.addTag({ name: 'twitter:card', content: 'summary_large_image' });
    this.meta.addTag({ name: 'twitter:site', content: '' });
    this.meta.addTag({ name: 'twitter:title', content: 'Wede Tech - Contact us' });
    this.meta.addTag({ name: 'twitter:description', content: 'Turn your website into a powerful marketing tool.' });
    this.meta.addTag({ name: 'twitter:image', content: 'https://wede.tech/assets/img/logo-rebrand.png' });
    this.meta.addTag({ name: 'og:title', content: 'Wede Tech - Contact us'});
    this.meta.addTag({ name: 'og:type', content: 'website'});
    this.meta.addTag({ name: 'og:url', content: 'https://wede.tech/contact-us'});
    this.meta.addTag({ name: 'og:image', content: 'https://wede.tech/assets/img/logo-rebrand.png'});
    this.meta.addTag({ name: 'og:description', content: 'Turn your website into a powerful marketing tool.'});
  }

  ngOnInit() {}

  onSubmit(message: any) {
    if(this.wtCForm.valid) {
      message = this.wtCForm.value;
      this.loading = true;
      this.es.sendMessage(message).subscribe(result => {
        console.log('Success from component.', result);
        this.loading = false;
        this.openCfInfo('Thank you for your message. We will get back to you as soon as possible', 'Close');
      })
    } else {
      console.log('Error from component');
      this.loading = false;
    }
  }

  openCfInfo(message: string, action: string) {
    let config = new MatSnackBarConfig();
    config.extraClasses = ['snackbar'];
    config.duration = 8000;
    this.cFormInfo.open(message, action, config);
  }

}
