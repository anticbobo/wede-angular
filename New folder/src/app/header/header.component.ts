import { Component, trigger, state, style, transition, animate, keyframes } from '@angular/core';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [
    trigger('isNavOpen', [
      state('true',  style({ display: 'block', visibility: 'visible', opacity: 1})),
      state('false', style({ display: 'none', visibility: 'hidden', opacity: 0})),
      transition('0 => 1', animate('500ms ease-in')),
      transition('1 => 0', animate('500ms ease-out'))
    ])
  ]
})
export class HeaderComponent {
  public isNavOpen: boolean = false;
  public isHamburgerOpen: boolean = false;

  toggleIsNavOpen() {
    this.isNavOpen = !this.isNavOpen;
    this.isHamburgerOpen = !this.isHamburgerOpen;
    if(this.isNavOpen) {
      document.body.classList.add('hide-overflow');
    } else {
      document.body.classList.remove('hide-overflow');
    }
  }
  
}