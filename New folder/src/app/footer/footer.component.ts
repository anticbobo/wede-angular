import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public year = new Date().getFullYear();
  navIsFixed: boolean;

    constructor(@Inject(DOCUMENT) private document: Document) { }

    @HostListener("window:scroll", []) onWindowScroll() {
      if (window.pageYOffset /* || document.documentElement.scrollTop || document.body.scrollTop */ > 250) {
        this.navIsFixed = true;
      } else if (this.navIsFixed && window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop < 100) { 
        this.navIsFixed = false; 
      } 
    } 
    scrollToTop() { 
      (function smoothscroll() { 
        var currentScroll = document.documentElement.scrollTop || document.body.scrollTop; if (currentScroll > 0) {
          window.requestAnimationFrame(smoothscroll);
          window.scrollTo(0, currentScroll - (currentScroll / 12));
        }
      })();
    }

  ngOnInit() {
  }

}
