import { Component, OnInit, Input } from '@angular/core';
import { TeamMember } from '../team-member.model';

@Component({
  selector: 'app-team-member',
  templateUrl: './team-member.component.html',
  styleUrls: ['./team-member.component.scss']
})
export class TeamMemberComponent implements OnInit {

  @Input() member: TeamMember;

  constructor() {}

  ngOnInit() {
  }

  showText() {
    if(this.member.facebook == '' && this.member.linkedin == '' && this.member.twitter == '' && this.member.instagram == '') {
      return true;
    } else {
      return false;
    }
  }

}
