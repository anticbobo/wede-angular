export class TeamMember {
    constructor(
        public img: string,
        public name: string,
        public position: string,
        public linkedin: string,
        public facebook: string,
        public instagram: string,
        public twitter: string){}
}