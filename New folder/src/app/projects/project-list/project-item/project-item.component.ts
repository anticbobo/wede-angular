import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Project } from './../../project.model';
import { ProjectDetailsComponent } from '../../project-details/project-details.component';

@Component({
  selector: 'app-project-item',
  templateUrl: './project-item.component.html',
  styleUrls: ['./project-item.component.scss']
})
export class ProjectItemComponent implements OnInit {
  @Input() project: Project;
  @Output() projectSelected = new EventEmitter<void>();

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    let dialogRef = this.dialog.open(ProjectDetailsComponent, {
      maxWidth: '100vw',
      width: '95vw',
      height: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
      
    });
  }

  ngOnInit() {
  }
  
  selectProject(project: Project) {
    this.projectSelected.emit();
  }

}