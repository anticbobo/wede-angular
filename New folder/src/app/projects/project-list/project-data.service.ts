import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import { Project, Technology } from './../project.model';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class ProjectDataService {
    private projectsJsonUrl = 'data/projects.json';

    constructor(private http: Http){}
    private project: Project = new Project('', {img1: '', img2: '', img3: '', img4: '', img5: '', img6: ''}, '', '', '', new Technology('', ''));
    private projectSource = new BehaviorSubject<Project>(this.project);
    currentProject = this.projectSource.asObservable();

    changeProject(project: Project) {
        this.projectSource.next(project);
    }

    getProjects():Observable<Project[]> {
        return this.http.get(this.projectsJsonUrl)
            .map(result => result.json().projects)
    }

}