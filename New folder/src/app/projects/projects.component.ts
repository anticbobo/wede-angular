import { Component, OnInit, Inject, EventEmitter, Output, Input } from '@angular/core';
import { Project } from './project.model';
import { ProjectDataService } from './project-list/project-data.service';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  project: Project;

  constructor(public data: ProjectDataService, private meta: Meta, private title: Title) {
    this.title.setTitle('Wede Tech - Our projects');
    this.meta.addTag({ name: 'author', content: 'Wede Tech'});
    this.meta.addTag({ name: 'description', content: 'Turn your website into a powerful marketing tool.'});
    this.meta.addTag({ name: 'twitter:card', content: 'summary_large_image' });
    this.meta.addTag({ name: 'twitter:site', content: '' });
    this.meta.addTag({ name: 'twitter:title', content: 'Wede Tech - Our projects' });
    this.meta.addTag({ name: 'twitter:description', content: 'Turn your website into a powerful marketing tool.' });
    this.meta.addTag({ name: 'twitter:image', content: 'https://wede.tech/assets/img/logo-rebrand.png' });
    this.meta.addTag({ name: 'og:title', content: 'Wede Tech - Our projects'});
    this.meta.addTag({ name: 'og:type', content: 'website'});
    this.meta.addTag({ name: 'og:url', content: 'https://wede.tech/projects'});
    this.meta.addTag({ name: 'og:image', content: 'https://wede.tech/assets/img/logo-rebrand.png'});
    this.meta.addTag({ name: 'og:description', content: 'Turn your website into a powerful marketing tool.'});
  }
  
  ngOnInit() {
    this.data.currentProject.subscribe(project => this.project = project);
  }
  
  sendProject(project: Project) {
    this.data.changeProject(project);
  }

}