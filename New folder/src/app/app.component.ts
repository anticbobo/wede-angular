import { Component, AfterViewInit } from '@angular/core';
import { trigger, animate, style, group, query, transition } from '@angular/animations';
import { NgsRevealConfig } from 'ng-scrollreveal';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements AfterViewInit {

  isPageLoaded = false;

  constructor(config: NgsRevealConfig) {
    config.duration = 1000;
    config.delay = 200;
    config.easing = 'cubic-bezier(0.645, 0.045, 0.355, 1)';
  }

  getState(outlet) {
    return outlet.activatedRouteData.state;
  }
  onActivate(event) {
    window.scroll(0, 0);
  }

  ngAfterViewChecked() {
    setTimeout(() => this.isPageLoaded = true, 100);
  }

  ngAfterViewInit() {
  }

}
