export const ROUTES = [
  '/',
  '/about-us',
  '/contact-us',
  'projects',
  'skills-services',
  'testimonials'
];
